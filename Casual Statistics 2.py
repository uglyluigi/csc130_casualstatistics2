###########################################################################################
# Name: Brennan Forrest
# Date: 10/16/17
# Description: Manipulates a list of 3 integers in various ways.
###########################################################################################

"""
Gets an integer from the user.
At least it expects it to be an integer.
"""
def get_integer():
	return input("Enter an integer: ")

"""
Returns the maximum values in a list of integers.
"""
def minimum(int_list):
	min = int_list[0]
	index = 0
	while index < len(int_list) - 1: #Iterate over the list until the index reaches len(list) - 1 since lists are 0-based
		if int_list[index] < min:
			min = int_list[index] #Set the newly found minimum
		index += 1
	return min

"""
Returns the maximum value in a list of integers.
"""
def maximum(int_list):
	max = int_list[0]
	index = 0
	while index < len(int_list) - 1:
		if int_list[index] > max:
			max = int_list[index]
		index += 1
	return max

"""
Returns the median value in a list of integers.
"""
def median(int_list):
	list_length = len(int_list)
	sorted_list = sorted(int_list) #Now, I know I asked about sorting the lists, but I was talking about using it for all other functions as well. Median requires a sorted list though.
	list_mid_index = (list_length - 1) // 2 #Finds the index of the element located in the center of the list. Floored division just in case the list is of an even size.

	if len(int_list) % 2 == 0:
		return (sorted_list[list_mid_index] + sorted_list[list_mid_index + 1]) / 2 #Averages the two middle integers in the case of an even-sized list.
	else:
		return sorted_list[list_mid_index]



"""
Returns the average of all values in a list of integers.
"""
def mean(int_list):
	sum = 0

	for i in int_list: #Add all the integers in the list together and divide by the length of the list.
		sum += i

	return 1.0 * sum / len(int_list)

"""
Returns the most commonly occuring integer in a list or None if 
all values are unique.
I'm actually quite proud of this function. I only had to look up
how to get a list of values and keys from a dictionary and a clever
way to check for uniques in a list with the set function.
"""
def mode(int_list):
	int_dict = {}

	for i in int_list: #For each integer in the list...
		if i in int_dict: #Check to see if the integer is in the dictionary...
			int_dict[i] += 1 #if it is, increment its number of occurences
		else:
			int_dict[i] = 1 #if it's not, put it in the dict with the default value 1

	"""
	Now this is a cool and creative way to check for uniques in a list since
	sets are collections that can only contain unique values. Because of this property,
	sets made from lists that end up only being 1 element in length are made from a list
	containing only identical values; since this list is the number of occurences each
	integer has inside the int_list, if the set of that list is 1, there is no mode.
	"""

	if len(set(int_list)) == 1: #This accounts for lists where all values are identical. Without this statement,
		return int_list[0] #if all values in int_list were identical, the program would still say there's no mode.
	elif len(set(int_dict.values())) > 1: #If there's an integer that occurs more than once in int_list...
		keys = list(int_dict.keys())
		mode = keys[0]

		for i in int_dict.iterkeys(): #Find out which one it is...
			if int_dict[i] > int_dict[mode]: #By comparing the current potential mode with each potential mode in the dict...
				mode = i #and overwriting it if the current number in the dict has fewer occurences than a different one in the dict!

		return mode
	else: #For when all values in the list are unique
		return None

"""
Finds the range of the list.
Simply subtracts the minimum from the maximum value of the list.
"""
def range(int_list):
	return maximum(int_list) - minimum(int_list)



###############################################
# MAIN PART OF THE PROGRAM
# implement the main part of your program below
# comments have been added to assist you
###############################################
# get three numbers from the user

int_list = []
int_list.append(get_integer()) 
int_list.append(get_integer()) 
int_list.append(get_integer())

# display the various statistics

print("The minimum value is {}.".format(minimum(int_list)))
print("The maximum value is {}.".format(maximum(int_list)))
print("The mean is {}.".format(mean(int_list)))
print("The median is {}.".format(median(int_list)))
mode = mode(int_list)
if mode is None:
	print("The mode is undefined.")
else:
	print("The mode is {}.".format(mode))
print("The range is {}.".format(range(int_list)))